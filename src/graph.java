import java.util.ArrayList;
import java.util.Stack;

public class graph {
    public int way_start = 0;
    public int way_end = 0;
    public ArrayList<vertex> ver_arr;
    graph(graph origin){
        ver_arr = new ArrayList<vertex>();
        for(int i = 0; i < origin.ver_arr.size(); i++){
            ver_arr.add(new vertex(origin.ver_arr.get(i)));
        }
    }
    graph(){
        ver_arr = new ArrayList<vertex>();
    }
    public graph substract(graph A, graph B){ //A - B
        graph res = new graph(A);
        for(int i = 0; i < B.ver_arr.size(); i++){
            for(int j = 0; j < B.ver_arr.get(i).branches.length; j++){
                if(B.ver_arr.get(i).branches[j] == 1){
                    res.ver_arr.get(i).branches[j] = 0;
                }
            }
            res.ver_arr.get(i).prop = 0;
            for(int j = 0; j < B.ver_arr.size(); j++){
                if(res.ver_arr.get(i).branches[j] == 1){
                    res.ver_arr.get(i).prop = 1;
                    break;
                }
            }

        }
        return res;
    }
    public void mark_free_vertex(){
        for(int i = 0; i< this.ver_arr.size(); i++) {
            this.ver_arr.get(i).prop = 0;
            for (int j = 0; j < this.ver_arr.size(); j++) {
                if (this.ver_arr.get(i).branches[j] == 1) {
                    this.ver_arr.get(i).prop = 1;
                    break;
                }
            }
        }
    }
    public void reset_vertext_status(){
        for(int i = 0; i <this.ver_arr.size(); i++){
            this.ver_arr.get(i).visited = 0;
            this.ver_arr.get(i).tin = 0;
        }
    }
    public Stack<Integer> get_son_id(int ver_id){
        Stack<Integer> res = new Stack<Integer>();
        if(ver_id == -1){
            return res;
        }
        for(int i = 0; i< this.ver_arr.get(ver_id).branches.length; i++){
            if(this.ver_arr.get(ver_id).branches[i] == 1){
                if(this.ver_arr.get(i).tin > this.ver_arr.get(ver_id).tin){
                    res.push(i);
                }
            }
        }
        return res;
    }
    public int get_father_id(int ver_id){
        if(ver_id == -1){
            return -1;
        }
        for(int i = 0; i< this.ver_arr.get(ver_id).branches.length; i++){
            if(this.ver_arr.get(ver_id).branches[i] == 1){
                if(this.ver_arr.get(i).tin < this.ver_arr.get(ver_id).tin){
                    return i;
                }
            }
        }
        return -1;
    }
    public boolean empty(){
        boolean res = true;
        for(int i = 0; i < this.ver_arr.size(); i++){
            for(int j = 0; j < this.ver_arr.size(); j++){
                if(this.ver_arr.get(i).branches[j] == 1){
                    res = false;
                }
            }
        }
        return res;
    }
    public int find_vertex_with_tin(int _tin){
        for(int i = 0; i < this.ver_arr.size(); i++){
            if(this.ver_arr.get(i).tin == _tin){
                return i;
            }
        }
        return -1;
    }
    public int fup(int ver_id, graph origin){
        if(ver_id == -1){
            return 1000000;
        }
        int fup_to = 1000000;
        int temp_fup_to;
        for(int i = 0; i< this.ver_arr.get(ver_id).branches.length; i++){
            if(this.ver_arr.get(ver_id).branches[i] == 1){
                if(this.ver_arr.get(i).tin > this.ver_arr.get(ver_id).tin){
                    temp_fup_to = fup(i,origin);
                    if(temp_fup_to < fup_to){
                        fup_to = temp_fup_to;
                    }
                }
            }
        }
        int tin_p = 1000000;
        int tin_tmp = 100;
        for(int i = 0; i<this.ver_arr.size(); i++){
            if(origin.ver_arr.get(ver_id).branches[i] == 1){
                if(this.ver_arr.get(ver_id).branches[i] == 0){
                    tin_tmp = this.ver_arr.get(i).tin;
                    if(tin_tmp < tin_p){
                        tin_p = tin_tmp;
                    }
                }
            }
        }
        int tin_v = this.ver_arr.get(ver_id).tin;
        if(fup_to < tin_p){
            if(tin_v < fup_to){
                return tin_v;
            }else{
                return fup_to;
            }
        }else{
            if(tin_v < tin_p){
                return tin_v;
            }else{
                return tin_p;
            }
        }
    }
    public graph sum(graph A, graph B){
        graph res = new graph(A);
        for(int i = 0; i < B.ver_arr.size(); i++){
            for(int j = 0; j < B.ver_arr.get(i).branches.length; j++){
                if(B.ver_arr.get(i).branches[j] == 1){
                    res.ver_arr.get(i).branches[j] = 1;
                }
            }
        }
        res.mark_free_vertex();
        return res;
    }
    public void set_vertex(vertex ori, int index){
        for(int i = 0; i < this.ver_arr.size();i++){
            this.ver_arr.get(index).branches[i] = ori.branches[i];
        }
    }
    public graph delete_free_vertex(){
        this.mark_free_vertex();
        int[] vertex_for_delete = new int[this.ver_arr.size()];
        int free_ver = 0;
        for(int i = 0; i<this.ver_arr.size();i++){
            if(this.ver_arr.get(i).prop == 0){
                free_ver++;
                vertex_for_delete[i] = 1;
            }
        }
        graph res = new graph();
        for(int i = 0; i < (this.ver_arr.size()-free_ver);i++){
            res.ver_arr.add(new vertex(i,1,(this.ver_arr.size() - free_ver)));
        }
        int space_x = 0;
        int space_y = 0;
        for(int i = 0; i< res.ver_arr.size();i++){
            while(vertex_for_delete[i+space_x] == 1){
                space_x = space_x + 1;
            }
            space_y = 0;
            for(int j = 0; j < res.ver_arr.size();j++){
                while(vertex_for_delete[j+space_y] == 1){
                    space_y = space_y + 1;
                }
                res.ver_arr.get(i).branches[j] = this.ver_arr.get(i+space_x).branches[j+space_y];
            }
        }
        return res;
    }
    public boolean way_belongs(graph way){
        int triger = 0;
        this.mark_free_vertex();
        way.mark_free_vertex();
        for(int i = 0; i < this.ver_arr.size(); i++){
            if((this.ver_arr.get(i).prop == 1)&&(way.ver_arr.get(i).prop == 1)){
                triger = triger + 1;
            }
        }
        if(triger == 2){
            return true;
        }else{
            return false;
        }
    }
}
