import java.util.ArrayList;
import java.util.Stack;

public class main {
    public static graph DFS(graph origin, int start_ver, int marker) {
        graph res = new graph();
        Stack<Integer> stk = new Stack<Integer>();
        for (int i = 0; i < origin.ver_arr.size(); i++) {
            res.ver_arr.add(new vertex(i, 1, origin.ver_arr.size()));
        }
        stk.push(origin.ver_arr.get(start_ver).vertex_id);
        int cur_ver = 0;
        int prev_ver = -1;
        int time = 0;
        while (!stk.empty()) {
            if (prev_ver != -1) {
                cur_ver = stk.pop();
                prev_ver = stk.pop();
                if (res.ver_arr.get(cur_ver).visited == marker) {
                    continue;
                }
                res.ver_arr.get(cur_ver).branches[prev_ver] = 1;
                res.ver_arr.get(prev_ver).branches[cur_ver] = 1;
            } else {
                prev_ver = 0;
                cur_ver = stk.pop();
            }
            res.ver_arr.get(cur_ver).tin = time;
            res.ver_arr.get(cur_ver).visited = marker;
            origin.ver_arr.get(cur_ver).visited = marker;
            time = time + 1;
            for (int i = 0; i < origin.ver_arr.get(cur_ver).branches.length; i++) {
                if ((res.ver_arr.get(i).visited == 0) && (origin.ver_arr.get(cur_ver).branches[i] == 1)) {
                    stk.push(cur_ver);
                    stk.push(i);
                }
            }
        }
        return res;
    }
    public static graph attempt_to_create_cycle(graph dfs, graph origin, int ver_num) {
        graph cycle = new graph();
        for (int k = 0; k < origin.ver_arr.size(); k++) {
            if ((origin.ver_arr.get(ver_num).branches[k] == 1) && (dfs.ver_arr.get(ver_num).branches[k] == 0)) {
                for (int i = 0; i < origin.ver_arr.size(); i++) {
                    cycle.ver_arr.add(new vertex(i, 1, origin.ver_arr.size()));
                }
                cycle.ver_arr.get(ver_num).branches[k] = 1;
                cycle.ver_arr.get(k).branches[ver_num] = 1;
                int cur_ver = ver_num;
                while (true) {
                    int temp_father = dfs.get_father_id(cur_ver);
                    cycle.ver_arr.get(cur_ver).branches[temp_father] = 1;
                    cycle.ver_arr.get(temp_father).branches[cur_ver] = 1;
                    if (temp_father == k) {
                        return cycle;
                    } else {
                        cur_ver = dfs.get_father_id(cur_ver);
                    }
                }
            }
        }
        return null;
    }

    public static graph Cycle(graph origin) {
        graph dfs = DFS(origin,0,1);
        graph res = null;
        for (int i = origin.ver_arr.size() - 1; i > -1; i--) {
            res = attempt_to_create_cycle(dfs, origin, dfs.find_vertex_with_tin(i));
            if (res != null) {
                return res;
            }
        }
        return res;
    }

    public static graph extract_way(graph origin, graph segments) {
        int chosen_vertex = -1;
        for (int i = 0; i < origin.ver_arr.size(); i++) {
            if ((segments.ver_arr.get(i).prop == 1) && (origin.ver_arr.get(i).prop == 1)) {
                chosen_vertex = i;
                break;
            }
        }
        if (chosen_vertex == -1) {
            return null;
        }
        graph res = new graph();
        res.way_start = chosen_vertex;
        Stack<Integer> stk = new Stack<Integer>();
        for (int i = 0; i < origin.ver_arr.size(); i++) {
            res.ver_arr.add(new vertex(i, 0, origin.ver_arr.size()));
        }
        stk.push(chosen_vertex);
        int cur_ver = chosen_vertex;
        int prev_ver = -1;
        int time = 0;
        while (!stk.empty()) {
            if (prev_ver != -1) {
                cur_ver = stk.pop();
                prev_ver = stk.pop();
                if (res.ver_arr.get(cur_ver).visited == 1) {
                    continue;
                }
                res.ver_arr.get(cur_ver).branches[prev_ver] = 1;
                res.ver_arr.get(prev_ver).branches[cur_ver] = 1;
                if ((segments.ver_arr.get(cur_ver).prop == 1) && (origin.ver_arr.get(cur_ver).prop == 1)) {
                    res.way_end = cur_ver;
                    return res;
                }
            } else {
                prev_ver = 0;
                cur_ver = stk.pop();
            }
            res.ver_arr.get(cur_ver).tin = time;
            res.ver_arr.get(cur_ver).visited = 1;
            time = time + 1;
            for (int i = 0; i < origin.ver_arr.get(cur_ver).branches.length; i++) {
                if ((res.ver_arr.get(i).visited == 0) && (segments.ver_arr.get(cur_ver).branches[i] == 1)) {
                    stk.push(cur_ver);
                    stk.push(i);
                }
            }
        }
        return res;
    }
    public static ArrayList<graph> step_gamma_algorithm(graph origin, graph way) {
        graph A1 = new graph();
        graph A2 = new graph();
        for (int i = 0; i < origin.ver_arr.size(); i++) {
            A1.ver_arr.add(new vertex(i, 0, origin.ver_arr.size()));
            A2.ver_arr.add(new vertex(i, 0, origin.ver_arr.size()));
        }
        A1 = origin.sum(A1, way);
        A2 = origin.sum(A2, way);
        int cur_ver = way.way_start;
        A1.ver_arr.get(cur_ver).visited = 1;
        while (true) {
            if (cur_ver == way.way_end) {
                break;
            }
            for (int i = 0; i < origin.ver_arr.size(); i++) {
                if ((origin.ver_arr.get(cur_ver).branches[i] == 1) && (A1.ver_arr.get(i).visited == 0)) {
                    A1.ver_arr.get(i).branches[cur_ver] = 1;
                    A1.ver_arr.get(cur_ver).branches[i] = 1;
                    A1.ver_arr.get(i).visited = 1;
                    cur_ver = i;
                    break;
                }
            }
        }
        cur_ver = way.way_start;
        A2.ver_arr.get(cur_ver).visited = 1;
        while (true) {
            if (cur_ver == way.way_end) {
                break;
            }
            for (int i = origin.ver_arr.size() - 1; i > -1; i--) {
                if ((origin.ver_arr.get(cur_ver).branches[i] == 1) && (A2.ver_arr.get(i).visited == 0)) {
                    A2.ver_arr.get(i).branches[cur_ver] = 1;
                    A2.ver_arr.get(cur_ver).branches[i] = 1;
                    A2.ver_arr.get(i).visited = 1;
                    cur_ver = i;
                    break;
                }
            }
        }
        ArrayList<graph> res = new ArrayList<graph>();
        res.add(A1);
        res.add(A2);
        return res;
    }
    public static graph lower_segment(graph origin, graph segments, ArrayList<graph> cls){
        ArrayList<graph> sgms = new ArrayList<graph>();
        graph tmp_segments = segments;
        while(!tmp_segments.empty()){
            sgms.add(extract_way(origin,tmp_segments));
            tmp_segments = origin.substract(tmp_segments,sgms.get(sgms.size() - 1));
        }
        int max = 0;
        int max_id = 0;
        int tmp_max = 0;
        for(int i = 0; i<sgms.size();i++){
            for(int j =0; j<cls.size();j++){
                if(cls.get(j).way_belongs(sgms.get(i))){
                    tmp_max++;
                }
            }
            if(tmp_max > max){
                max = tmp_max;
                max_id = i;
            }
        }
        return sgms.get(max_id);
    }
    public static boolean check_planarity(graph origin){
        ArrayList<graph> cls = new ArrayList<graph>();
        ArrayList<graph> cls_temp = new ArrayList<graph>();
        cls.add(Cycle(origin));
        cls.add(Cycle(origin));
        graph segments = origin.substract(origin,cls.get(0));
        graph cur_way;
        int triger = 0;
        while(true){
            if(segments.empty()){
                return true;
            }
            triger = 0;
            cur_way = lower_segment(origin,segments,cls);
            segments = origin.substract(segments,cur_way);
            for(int i = 0; i < cls.size(); i++){
                cls.get(i).mark_free_vertex();
                if(cls.get(i).way_belongs(cur_way)){
                   cls_temp = step_gamma_algorithm(cls.get(i),cur_way);
                   cls.remove(i);
                   triger = 1;
                   break;
                }
            }
            if(triger == 0){
                System.out.print('\n');
                return false;
            }
            cls.add(cls_temp.get(0));
            cls.add(cls_temp.get(1));
        }
    }
    public static ArrayList<graph> components(graph origin){
        graph dfs;
        graph tmp = new graph();
        ArrayList<graph> res = new ArrayList<graph>();
        for(int i = 0; i<origin.ver_arr.size();i++){
            if(origin.ver_arr.get(i).visited == 0){
                tmp = new graph();
                dfs = DFS(origin,i,i+1);
                for(int k = 0; k<origin.ver_arr.size();k++){
                    tmp.ver_arr.add(new vertex(k,0,origin.ver_arr.size()));
                }
                for(int j = 0; j < origin.ver_arr.size();j++){
                    if(dfs.ver_arr.get(j).visited == i+1){
                        tmp.set_vertex(origin.ver_arr.get(j),j);
                    }
                }
                tmp = tmp.delete_free_vertex();
                res.add(tmp);
            }
        }
        return res;
    }
    public static void cut_branches(graph origin){
        graph DFS = DFS(origin,0,1);
        for (int i = 0; i < origin.ver_arr.size(); i++) {
            Stack<Integer> tmp = DFS.get_son_id(i);
            if (tmp.empty()) {
                continue;
            }
            int temp_son;
            while (!tmp.empty()) {
                temp_son = tmp.pop();
                if (DFS.fup(temp_son, origin) > DFS.ver_arr.get(i).tin) {
                    origin.ver_arr.get(temp_son).branches[i] = 0;
                    origin.ver_arr.get(i).branches[temp_son] = 0;
                }
            }
        }
    }
    public static void main(String[] args) {
        graph origin = new graph();
        origin.ver_arr.add(new vertex(0, new int[]{0,1,1,1,1,0,0,0}, 1));
        origin.ver_arr.add(new vertex(1, new int[]{1,0,1,1,1,0,1,0}, 1));
        origin.ver_arr.add(new vertex(2, new int[]{1,1,0,1,1,0,0,0}, 1));
        origin.ver_arr.add(new vertex(3, new int[]{1,1,1,0,1,0,0,0}, 1));
        origin.ver_arr.add(new vertex(4, new int[]{1,1,1,1,0,0,0,0}, 1));
        origin.ver_arr.add(new vertex(5, new int[]{0,0,0,0,0,0,1,1}, 1));
        origin.ver_arr.add(new vertex(6, new int[]{0,1,0,0,0,1,0,1}, 1));
        origin.ver_arr.add(new vertex(7, new int[]{0,0,0,0,0,1,1,0}, 1));
        cut_branches(origin);
        for (int i = 0; i < origin.ver_arr.size(); i++) {
            System.out.print('\n');
            for (int j = 0; j < origin.ver_arr.size(); j++) {
                System.out.print(" " + origin.ver_arr.get(i).branches[j] + ", ");
            }
        }
        System.out.print('\n');
        origin.reset_vertext_status();
        ArrayList<graph> components = components(origin);
        boolean planarity = false;
        for(int i = 0; i< components.size();i++) {
            if (components.get(i).ver_arr.size() != 0) {
                planarity = check_planarity(components.get(i));
            }else{
                planarity = true;
            }
            if (planarity) {
                System.out.println("PLANARITY TRUE");
            } else {
                System.out.println("PLANARITY FALSE");
            }
        }
    }
}
