

public class vertex {
    public int prop;
    public int tin = 0;
    public int visited = 0;
    public int vertex_id;
    public int[] branches;
    vertex(int _id, int[] _branches, int prop_){
        prop = prop_;
        vertex_id = _id;
        branches = _branches;
    }
    vertex(int _id, int _prop,int size){
        prop = _prop;
        vertex_id = _id;
        branches = new int[size];
    }
    vertex(vertex origin) {
        tin  = origin.tin;
        visited = origin.visited;
        prop = origin.prop;
        vertex_id = origin.vertex_id;
        branches = new int[origin.branches.length];
        for (int i = 0; i < origin.branches.length; i++) {
            branches[i] = origin.branches[i];
        }
    }
}
